#!/usr/bin/env python
import random

bulls = 0
cows = 0

def GeneraterandNum(n):
    assert n <= 10
    l = list(range(10)) # compat py2 & py3
    random.shuffle(l)
    return (l)

def validate(n):
    if n == "x" or n == "?":
        return 1
    if not n.isdigit():
        print "Enter only numbers"
        return 0
    if len(n) > 4:
        print "Enter only 4 chars\n"
        return 0
    if len(n) != len(set(n)):
        print "Enter only unique chars\n"
        return 0
    return 1

def checkNumber(n, r):
    k = 0
    i = 0
    global bulls
    global cows
    for i in n:
        if (r.find(i) != -1):
            if (n[k] == r[k]):
                bulls = bulls+1
            else:
                cows = cows+1
        k = k+1

print 'Welcome to GuessMyPIN'
print '=====================\n\n'
num = input('Enter number of digits: ')
j = GeneraterandNum(num)
m = ''.join(str(d) for d in j[:num])

count = 1
print("Press ? for help, x for exit, or type the number if you guessed it\n")
while (1):
    opt = raw_input("Guess#{0}:\t".format(count))
    
    if (validate(str(opt)) == 0):
        continue
    
    if opt == "?":
        print "Bulls - exact location match\n Cows - Number matched at diff location\n Pigs - Nothing matched\n"
        continue
    elif opt == "x":
        print "Better Luck, next time. My PIN is ", m
        break
    elif int(opt) == int(m):
        
        if count == 1:
            print "Outstanding, You are a guessing miracle"
        elif count < 5:
            print "Excellent, You are Genius"
        elif count < 10:
            print "Very good, Keep it up"
        elif count < 15:
            print "Good, Keep improving"
        else:
            print "Finally, You made it"
        print " You got it right @ {0} guesses\n".format(count)

        break
    else:
        bulls = 0
        cows = 0
        checkNumber(str(opt), m)
        if (bulls == 0 and cows == 0):
            print "Pigs, try another guess"
        else:
            print "{0} Bulls, {1} cows\n".format(bulls, cows)
        count = count+1



